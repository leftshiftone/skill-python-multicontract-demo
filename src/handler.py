from skill_core_tools.skill_log_factory import SkillLogFactory
from skill_core_tools.skill_namespace import Endpoints

from src.adder import Adder
from src.tokenizer import NaiveTokenizer
from src.type_handler import TypeHandler

log = SkillLogFactory.get_logger("handler")

# handlers for specific contracts can be registered using the Endpoints class from the skill_core_tools library
# or by manually extracting the namespace information from the context (context["namespace"])
handlers = Endpoints()
handlers.add("demo.echo", lambda request, _ctx: {'response': request['text']})
handlers.add("demo.adder", lambda request, _ctx: {'result': Adder.add(request['a'], request['b'])})
handlers.add("leftshiftone/tokenizer", lambda request, _ctx: {'tokens': NaiveTokenizer.tokenize(request['text'])})
handlers.add("demo.simpletype", TypeHandler.log_and_echo)
handlers.add("demo.complextype", TypeHandler.log_and_echo)


def evaluate(payload: dict, context: dict) -> dict:
    log.debug(f"received a message: {payload}")
    log.debug(f"context: {context}")

    return handlers.handle(payload, context)


def on_started(_context):
    log.info("on_started triggered")