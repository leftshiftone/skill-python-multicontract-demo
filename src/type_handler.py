from skill_core_tools.skill_log_factory import SkillLogFactory


class TypeHandler:
    log = SkillLogFactory.get_logger(__name__)

    @staticmethod
    def log_and_echo(payload: dict, _ctx: dict) -> dict:
        TypeHandler.log.info(f"Received: {payload}")
        payload.pop(":namespace")
        return payload
